use libviz::plug;

#[plug]
pub mod plugin {

    use std::{any::Any, fmt::Debug};

    use libviz::{Int, List};
    use runtime::Backend;

    pub fn range(start: Int, end: Int) -> List<Int> {
        let mut result = vec![];
        for i in start.data..end.data {
            result.push(Int { data: i });
        }
        List { data: result }
    }

    pub fn say_something(channel: &Backend) {
        channel
            .send(runtime::OutputEvent::Speech {
                speaker: "Lain".to_string(),
                text: "pog".to_string(),
            })
            .unwrap();
    }

    pub fn debug<T: Any + Debug>(value: T) {
        let message = format!("DEBUG {}: {:?}", std::any::type_name::<T>(), value);
        println!("{}", message);
    }
}
