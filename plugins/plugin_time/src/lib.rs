#[libviz::plug]
pub mod plugin {

    use chrono::{Local, NaiveDateTime};
    use libviz::{CanSpeech, Int, Str};
    use runtime::Backend;

    #[derive(Debug, Clone, Copy)]
    pub struct Datetime {
        pub stamp: Int,
    }

    #[derive(Debug, Clone, Copy)]
    pub struct Instant {
        inner: std::time::Instant,
    }

    #[derive(Debug, Clone, Copy)]
    pub struct Duration {
        pub nanos: Int,
    }

    impl Datetime {
        fn into(&self) -> NaiveDateTime {
            NaiveDateTime::from_timestamp_opt(self.stamp.data, 0).unwrap()
        }
        pub fn new(stamp: Int) -> Datetime {
            Datetime { stamp }
        }
        pub fn now(channel: &Backend) -> Datetime {
            let local_time = Local::now();
            Datetime::new(
                channel,
                Int {
                    data: local_time.timestamp(),
                },
            )
        }
        pub fn format(&self, fmt: Str) -> Str {
            let text = self.into().format(&fmt.data);
            Str {
                data: text.to_string(),
            }
        }
    }

    impl Instant {
        pub fn now() -> Instant {
            Instant {
                inner: std::time::Instant::now(),
            }
        }
        pub fn elapsed(&self) -> Duration {
            Duration {
                nanos: Int {
                    data: self.inner.elapsed().as_nanos() as i64,
                },
            }
        }
    }

    impl CanSpeech for Duration {
        fn speech(&self) -> String {
            self.nanos.speech()
        }
    }

    pub fn heavy_operation() {
        std::thread::sleep(std::time::Duration::from_millis(1000));
    }
}
