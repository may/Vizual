use crate::{
    include::{Keyword, Operator},
    lexer::{Token, TokenType},
};

pub struct Parser {
    tokens: Vec<Token>,
    index: usize,
}

impl Parser {
    pub fn new(tokens: Vec<Token>) -> Self {
        Self { tokens, index: 0 }
    }
    fn next(&mut self) -> &Token {
        self.index += 1;
        &self.tokens[self.index]
    }
    fn peek(&mut self) -> &Token {
        &self.tokens[self.index + 1]
    }
    fn done(&self) -> bool {
        self.index >= self.tokens.len() - 1
    }
    fn expect(&mut self, token: TokenType) {
        let next = self.next().clone().tt;
        if next != token {
            panic!("Expect failed, expected {:?}, found {:?}", token, next)
        }
    }
    fn parse(&mut self) -> Vec<Statement> {
        let mut result = vec![];
        while !self.done() && self.peek().tt != TokenType::Keyword(Keyword::End) {
            let statement = self.parse_statement();
            result.push(statement);
        }
        result
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Expression {
    FunctionDef {
        inner: Vec<Statement>,
    },
    FunctionCall {
        item: Box<Expression>,
        arguments: Vec<Expression>,
    },
    TypeCall {
        item: Box<Expression>,
        arguments: Vec<(String, Expression)>,
    },
    Menu {
        options: Vec<(Expression, Statement)>,
    },
    Integer {
        value: i64,
    },
    String {
        value: String,
    },
    Identifier {
        value: String,
    },
    BinaryOp {
        lhs: Box<Expression>,
        operator: Operator,
        rhs: Box<Expression>,
    },
    If {
        condition: Box<Expression>,
        inner: Vec<Statement>,
    },
    List {
        inner: Vec<Expression>,
    },
    For {
        binding: Box<Expression>,
        iterator: Box<Expression>,
        inner: Vec<Statement>,
    },
    Use {
        target: Box<Expression>,
    },
    Defer {
        inner: Vec<Statement>,
    },
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Statement {
    Assign {
        item: Box<Expression>,
        value: Box<Expression>,
    },
    Speech {
        item: Box<Expression>,
        text: Box<Expression>,
    },
    Expression {
        expr: Box<Expression>,
    },
}

impl Parser {
    fn parse_type_arguments(&mut self) -> Vec<(String, Expression)> {
        match self.peek().tt {
            TokenType::CloseBrace => {
                self.expect(TokenType::CloseBrace);
                vec![]
            }
            _ => {
                let key = self.next().clone().tt.ident();
                self.expect(TokenType::Assign);
                let value = self.parse_expression();
                if self.peek().tt == TokenType::Comma {
                    self.expect(TokenType::Comma);
                }
                let next_argument = (key, value);
                let mut remaining = self.parse_type_arguments();
                remaining.insert(0, next_argument);
                remaining
            }
        }
    }

    fn parse_function_arguments(&mut self) -> Vec<Expression> {
        match self.peek().tt {
            TokenType::CloseBracket => {
                self.expect(TokenType::CloseBracket);
                vec![]
            }
            _ => {
                let expr = self.parse_expression();
                if self.peek().tt == TokenType::Comma {
                    self.expect(TokenType::Comma);
                }
                let mut remaining = self.parse_function_arguments();
                remaining.insert(0, expr);
                remaining
            }
        }
    }

    fn parse_list_arguments(&mut self) -> Vec<Expression> {
        match self.peek().tt {
            TokenType::CloseSquare => {
                vec![]
            }
            _ => {
                let expr = self.parse_expression();
                if self.peek().tt == TokenType::Comma {
                    self.expect(TokenType::Comma);
                }
                let mut remaining = self.parse_list_arguments();
                remaining.insert(0, expr);
                remaining
            }
        }
    }

    fn parse_menu_options(&mut self) -> Vec<(Expression, Statement)> {
        match self.peek().tt {
            TokenType::Keyword(Keyword::End) => {
                vec![]
            }
            _ => {
                let option_name = self.parse_expression();
                self.expect(TokenType::ThinArrow);
                let option_result = self.parse_statement();
                let option = (option_name, option_result);
                let mut remaining = self.parse_menu_options();
                remaining.insert(0, option);
                remaining
            }
        }
    }

    fn parse_atom(&mut self) -> Expression {
        let next = self.next().clone();
        match next.tt {
            TokenType::IntLiteral(value) => Expression::Integer { value },
            TokenType::StringLiteral(value) => Expression::String { value },
            TokenType::Identifier(value) => Expression::Identifier { value },
            TokenType::Keyword(Keyword::Def) => {
                let inner = self.parse();
                self.expect(TokenType::Keyword(Keyword::End));
                Expression::FunctionDef { inner }
            }
            TokenType::Keyword(Keyword::If) => {
                let condition = Box::new(self.parse_expression());
                self.expect(TokenType::Keyword(Keyword::Then));
                let inner = self.parse();
                self.expect(TokenType::Keyword(Keyword::End));
                Expression::If { condition, inner }
            }
            TokenType::Keyword(Keyword::For) => {
                let binding = Box::new(self.parse_expression());
                self.expect(TokenType::Keyword(Keyword::In));
                let iterator = Box::new(self.parse_expression());
                self.expect(TokenType::Keyword(Keyword::Do));
                let inner = self.parse();
                self.expect(TokenType::Keyword(Keyword::End));
                Expression::For {
                    binding,
                    iterator,
                    inner,
                }
            }
            TokenType::Keyword(Keyword::Menu) => {
                let options = self.parse_menu_options();
                self.expect(TokenType::Keyword(Keyword::End));
                Expression::Menu { options }
            }
            TokenType::Keyword(Keyword::Defer) => {
                let inner = self.parse();
                self.expect(TokenType::Keyword(Keyword::End));
                Expression::Defer { inner }
            }
            TokenType::OpenBracket => {
                let inner = self.parse_expression();
                self.expect(TokenType::CloseBracket);
                inner
            }
            TokenType::OpenSquare => {
                let inner = self.parse_list_arguments();
                self.expect(TokenType::CloseSquare);
                Expression::List { inner }
            }
            TokenType::Keyword(Keyword::Use) => {
                let target = Box::new(self.parse_expression());
                Expression::Use { target }
            }
            _ => panic!("Unexpected token: {:?}", next),
        }
    }

    fn parse_expression_part(&mut self, lhs: Expression) -> Expression {
        match self.peek().clone().tt {
            TokenType::Semicolon
            | TokenType::Comma
            | TokenType::Assign
            | TokenType::CloseBrace
            | TokenType::CloseBracket
            | TokenType::CloseSquare
            | TokenType::ThinArrow
            | TokenType::Keyword(Keyword::Then)
            | TokenType::Keyword(Keyword::In)
            | TokenType::Keyword(Keyword::Do)
            | TokenType::Colon => lhs,
            TokenType::Operator(_) => {
                let TokenType::Operator(operator) = self.next().clone().tt else {
                    panic!("Unreachable")
                };
                let rhs = self.parse_atom();
                let expr = Expression::BinaryOp {
                    lhs: Box::new(lhs),
                    operator,
                    rhs: Box::new(rhs),
                };
                self.parse_expression_part(expr)
            }
            TokenType::OpenBrace => {
                self.expect(TokenType::OpenBrace);
                let arguments = self.parse_type_arguments();
                let expr = Expression::TypeCall {
                    item: Box::new(lhs),
                    arguments,
                };
                self.parse_expression_part(expr)
            }
            TokenType::OpenBracket => {
                self.expect(TokenType::OpenBracket);
                let arguments = self.parse_function_arguments();
                let expr = Expression::FunctionCall {
                    item: Box::new(lhs),
                    arguments,
                };
                self.parse_expression_part(expr)
            }
            _ => panic!("Unexpected token: {:?}", self.peek()),
        }
    }

    pub fn parse_expression(&mut self) -> Expression {
        let lhs = self.parse_atom();
        self.parse_expression_part(lhs)
    }

    fn parse_statement(&mut self) -> Statement {
        let start = self.parse_expression();
        match self.peek().clone().tt {
            // Statement is already over, containing a single expression
            TokenType::Semicolon => {
                self.expect(TokenType::Semicolon);
                Statement::Expression {
                    expr: Box::new(start),
                }
            }
            // Speech statement
            TokenType::Colon => {
                self.expect(TokenType::Colon);
                let text = self.parse_expression();
                self.expect(TokenType::Semicolon);
                Statement::Speech {
                    item: Box::new(start),
                    text: Box::new(text),
                }
            }
            // Assignment statement
            TokenType::Assign => {
                self.expect(TokenType::Assign);
                let value = self.parse_expression();
                self.expect(TokenType::Semicolon);
                Statement::Assign {
                    item: Box::new(start),
                    value: Box::new(value),
                }
            }
            _ => panic!("Unexpected token: {:?}", self.peek()),
        }
    }
}

// Statements are
//   assignment
//   speech
//   if
//   for
//   while
//
// Expression are
//   function def
//   function call
//   literal
//   binary op
//   unary op
//   type instance
//
// def must be followed by Block
//
// ident must be followed by:
//   Operator (expr)
//   Open bracket (function call)
//   Colon (speech)
//   Equals (assignment)
// Any other token will end the section
//
// Literal must be followed by:
//   Operator (expr)
//

pub fn parse(tokens: Vec<Token>) -> Vec<Statement> {
    let mut parser = Parser::new(tokens);
    parser.parse()
}
