use std::{
    env, fs,
    io::Read,
    process::{Command, Stdio},
    time::Instant,
};

use clap::{Parser, Subcommand};
use colored::Colorize;
use serde::Deserialize;

mod compiler;
mod include;
mod lexer;
mod parser;
mod preprocessor;

#[derive(Parser)]
struct Args {
    #[command(subcommand)]
    command: Instr,
}

#[derive(Subcommand)]
enum Instr {
    New { name: String },
    Build,
    Run,
}

fn default_toml(name: &str) -> String {
    format!(
        "
name = \"{}\"
version = \"0.0.1\"
build = \"out\"
src = \"src/main.viz\"
plugins = []
",
        name
    )
}

const PLUGIN_FOLDER: &str = "/home/may/programming/projects/vizual/plugins";

fn default_out(plugins: &[String]) -> String {
    let mut m = "[package]
name = \"out\"
version = \"0.0.0\"
edition = \"2021\"

[dependencies]
runtime = { path = \"/home/may/programming/projects/vizual/runtime\" }
libviz = { path = \"/home/may/programming/projects/vizual/libviz\" }
"
    .to_string();
    for plugin in plugins {
        m.push_str(&format!(
            "plugin_{} = {{ path = \"{}/plugin_{}\" }}\n",
            plugin, PLUGIN_FOLDER, plugin
        ));
    }
    m
}

#[derive(Deserialize)]
struct VizConfig {
    name: String,
    version: String,
    build: String,
    src: String,
    plugins: Vec<String>,
}

fn change_dir(path: &str) {
    let mut current = env::current_dir().unwrap();
    current.push(path);
    env::set_current_dir(current).unwrap();
}

fn pop_dir() {
    let mut current = env::current_dir().unwrap();
    current.pop();
    env::set_current_dir(current).unwrap();
}

fn create_project(name: String) {
    fs::create_dir(&name).unwrap();
    change_dir(&name);
    fs::create_dir("src").unwrap();
    fs::write("src/main.viz", "").unwrap();
    fs::write("viz.toml", default_toml(&name)).unwrap();

    Command::new("cargo")
        .arg("new")
        .arg("out")
        .spawn()
        .unwrap()
        .wait()
        .unwrap();

    fs::write("out/Cargo.toml", default_out(&[])).unwrap();
}

fn build_project() {
    let config = fs::read_to_string("viz.toml").unwrap();
    let config: VizConfig = toml::from_str(&config).unwrap();
    println!(
        "   {} {} v{}",
        "Compiling".green().bold(),
        config.name,
        config.version
    );

    let start = Instant::now();

    fs::write("out/Cargo.toml", default_out(&config.plugins)).unwrap();

    let source = fs::read_to_string(&config.src).unwrap();
    let (text, _) = preprocessor::process(&source);
    println!("    {} preprocessor", "Finished".green().bold());

    let tokens = lexer::lex(&text);
    println!("    {} lexer", "Finished".green().bold());

    let nodes = parser::parse(tokens);
    println!("    {} parser", "Finished".green().bold());

    compiler::compile(
        nodes,
        &format!("{}/src/main.rs", config.build),
        config.plugins,
    );
    println!(
        "    {} compilation in {}s",
        "Finished".green().bold(),
        start.elapsed().as_secs_f32()
    );

    change_dir(&config.build);
    Command::new("cargo")
        .arg("build")
        .arg("--release")
        .spawn()
        .unwrap()
        .wait()
        .unwrap();

    println!("       {} program binary", "Built".green().bold());

    fs::rename("./target/release/out", "../bin").unwrap();
    pop_dir();

    println!(
        "       {} complete in {}s",
        "Build".green().bold(),
        start.elapsed().as_secs_f32()
    );
}

fn run_project() {
    println!("     {}", "Running".green().bold());

    let proc = Command::new("./bin")
        .stdout(Stdio::piped())
        .spawn()
        .unwrap();

    let mut pipe = proc.stdout.unwrap();
    'io: loop {
        let mut line = String::new();
        let mut c = [0; 1];
        'line: loop {
            if let Ok(_) = pipe.read_exact(&mut c) {
                match c {
                    [0x000A] => break 'line,
                    [c] => line.push(c as char),
                }
            } else {
                break 'io;
            }
        }
        println!("{}", line);
    }
}

fn main() {
    let args = Args::parse();
    match args.command {
        Instr::New { name } => create_project(name),
        Instr::Build => build_project(),
        Instr::Run => {
            build_project();
            run_project();
        }
    }

    // let raw = include_str!("../main.viz");
    // let (text, plugins) = preprocessor::process(raw);
    // let tokens = lexer::lex(&text);
    // let nodes = parser::parse(tokens);
    // compiler::compile(nodes, "out.rs");
}
