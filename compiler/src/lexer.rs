use crate::include::{Keyword, Operator};

#[derive(Debug, Clone, PartialEq)]
pub enum TokenType {
    Keyword(Keyword),
    Operator(Operator),
    Identifier(String),
    StringLiteral(String),
    IntLiteral(i64),

    OpenBrace,
    CloseBrace,
    OpenBracket,
    CloseBracket,
    OpenSquare,
    CloseSquare,

    Assign,
    Semicolon,
    Colon,
    Comma,
    ThinArrow,
    FatArrow,

    FSTART,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Token {
    pub tt: TokenType,
    pub row: usize,
    pub col: usize,
}

impl TokenType {
    pub fn ident(self) -> String {
        if let TokenType::Identifier(ident) = self {
            ident
        } else {
            panic!("Expected ident, found {:?}", self)
        }
    }
}

struct Lexer {
    text: Vec<char>,
    index: usize,
    tokens: Vec<Token>,
    row: usize,
    col: usize,
}

impl Lexer {
    fn new(text: impl Into<String>) -> Lexer {
        Lexer {
            text: text.into().chars().collect(),
            index: 0,
            tokens: vec![],
            row: 0,
            col: 0,
        }
    }
    fn get(&self) -> Option<&char> {
        self.text.get(self.index)
    }
    fn peek(&self) -> Option<&char> {
        self.text.get(self.index + 1)
    }
    fn single(&mut self, token: TokenType) {
        self.add_token(token);
        self.index += 1;
    }
    fn advance(&mut self, amount: usize) {
        for _ in 0..amount {
            if let Some('\n') = self.get() {
                self.row += 1;
                self.col = 0;
            } else {
                self.col += 1;
            }
            self.index += 1;
        }
    }
    fn double(&mut self, first_condition: char, first: TokenType, second: TokenType) {
        match self.peek() {
            Some(chr) => {
                if *chr == first_condition {
                    self.add_token(first);
                    self.advance(2);
                } else {
                    self.add_token(second);
                    self.advance(1);
                }
            }
            None => panic!("EOF reached partway through token"),
        }
    }
    fn token_equal(&mut self) {
        match self.peek() {
            Some(chr) => match chr {
                '=' => {
                    self.add_token(TokenType::Operator(Operator::Equal));
                    self.advance(2);
                }
                '>' => {
                    self.add_token(TokenType::FatArrow);
                    self.advance(2);
                }
                _ => {
                    self.add_token(TokenType::Assign);
                    self.advance(1);
                }
            },
            None => panic!("EOF reached partway through token"),
        }
    }

    fn token_minus(&mut self) {
        match self.peek() {
            Some(chr) => match chr {
                '=' => {
                    self.add_token(TokenType::Operator(Operator::SubAssign));
                    self.advance(2);
                }
                '>' => {
                    self.add_token(TokenType::ThinArrow);
                    self.advance(2);
                }
                _ => {
                    self.add_token(TokenType::Operator(Operator::Subtract));
                    self.advance(1);
                }
            },
            None => panic!("EOF reached partway through token"),
        }
    }

    fn add_token(&mut self, tt: TokenType) {
        self.tokens.push(Token {
            tt,
            row: self.row,
            col: self.col,
        });
    }

    fn comment(&mut self) {
        while let Some(chr) = self.get() {
            if *chr == '\n' {
                return;
            } else {
                self.advance(1);
            }
        }
    }

    fn build_number(&mut self) {
        let mut result = String::new();
        'builder: while let Some(chr) = self.get() {
            if !matches!(chr, '0'..='9') {
                break 'builder;
            }
            result.push(*chr);
            self.advance(1);
        }
        let number: i64 = result.parse().unwrap();
        self.add_token(TokenType::IntLiteral(number));
    }

    fn build_string(&mut self) {
        let mut result = String::new();
        self.advance(1);

        'builder: while let Some(chr) = self.get() {
            if *chr == '"' {
                break 'builder;
            }
            result.push(*chr);
            self.advance(1);
        }

        self.advance(1);
        self.add_token(TokenType::StringLiteral(result));
    }

    fn build_word(&mut self) {
        let mut result = String::new();
        'builder: while let Some(chr) = self.get() {
            if !matches!(chr, 'a'..='z' | 'A'..='Z' | '_') {
                break 'builder;
            }
            result.push(*chr);
            self.advance(1);
        }
        match result.as_str() {
            "if" => self.add_token(TokenType::Keyword(Keyword::If)),
            "def" => self.add_token(TokenType::Keyword(Keyword::Def)),
            "menu" => self.add_token(TokenType::Keyword(Keyword::Menu)),
            "end" => self.add_token(TokenType::Keyword(Keyword::End)),
            "then" => self.add_token(TokenType::Keyword(Keyword::Then)),
            "for" => self.add_token(TokenType::Keyword(Keyword::For)),
            "in" => self.add_token(TokenType::Keyword(Keyword::In)),
            "do" => self.add_token(TokenType::Keyword(Keyword::Do)),
            "use" => self.add_token(TokenType::Keyword(Keyword::Use)),
            "defer" => self.add_token(TokenType::Keyword(Keyword::Defer)),
            _ => self.add_token(TokenType::Identifier(result)),
        }
    }

    fn op_or_assign(&mut self, a: Operator, b: Operator) {
        match self.peek() {
            Some('=') => {
                self.advance(2);
                self.add_token(TokenType::Operator(b));
            }
            _ => {
                self.advance(1);
                self.add_token(TokenType::Operator(a));
            }
        }
    }

    fn lex(&mut self) {
        while let Some(chr) = self.get() {
            match chr {
                // Skip characters
                '#' => self.comment(),
                ' ' | '\t' | '\n' => self.advance(1),

                // Single character tokens
                '(' => self.single(TokenType::OpenBracket),
                ')' => self.single(TokenType::CloseBracket),
                '{' => self.single(TokenType::OpenBrace),
                '}' => self.single(TokenType::CloseBrace),
                '[' => self.single(TokenType::OpenSquare),
                ']' => self.single(TokenType::CloseSquare),
                ';' => self.single(TokenType::Semicolon),
                ',' => self.single(TokenType::Comma),
                '|' => self.single(TokenType::Operator(Operator::Or)),
                '&' => self.single(TokenType::Operator(Operator::And)),
                '+' => self.op_or_assign(Operator::Add, Operator::AddAssign),
                '*' => self.op_or_assign(Operator::Multiply, Operator::MulAssign),
                '/' => self.op_or_assign(Operator::Divide, Operator::DivAssign),
                '.' => self.single(TokenType::Operator(Operator::Dot)),

                // Two character tokens
                ':' => self.double(
                    ':',
                    TokenType::Operator(Operator::ColonColon),
                    TokenType::Colon,
                ),
                '!' => self.double(
                    '=',
                    TokenType::Operator(Operator::NotEqual),
                    TokenType::Operator(Operator::Not),
                ),
                '>' => self.double(
                    '=',
                    TokenType::Operator(Operator::GreaterOrEqual),
                    TokenType::Operator(Operator::Greater),
                ),
                '<' => self.double(
                    '=',
                    TokenType::Operator(Operator::LessOrEqual),
                    TokenType::Operator(Operator::Less),
                ),
                '-' => self.token_minus(),
                '=' => self.token_equal(),

                '0'..='9' => self.build_number(),
                '"' => self.build_string(),
                'a'..='z' | 'A'..='Z' | '_' => self.build_word(),

                _ => panic!("Unexpected character: `{}`", chr),
            }
        }
    }
}

pub fn lex(text: &str) -> Vec<Token> {
    let mut lexer = Lexer::new(text);
    lexer.lex();
    let mut result = lexer.tokens;
    result.insert(
        0,
        Token {
            tt: TokenType::FSTART,
            row: 0,
            col: 0,
        },
    );
    result
}
