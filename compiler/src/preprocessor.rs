use std::{collections::HashSet, fs};

fn run_command(result: &mut String, plugins: &mut HashSet<String>, command: &str, args: Vec<&str>) {
    match command {
        "include" => {
            let file_path = args[0];
            let file_text = fs::read_to_string(&file_path).unwrap();
            let (processed_text, new_plugins) = process(&file_text);
            result.push_str(&processed_text);
            result.push('\n');
            plugins.extend(new_plugins);
        }
        "plugin" => {
            let plugin_name = args[0];
            plugins.insert(plugin_name.to_string());
        }
        x @ _ => panic!("Unknown preprocessor command: {:?}", x),
    }
}

pub fn process(raw: &str) -> (String, HashSet<String>) {
    let mut result = String::new();
    let mut plugins = HashSet::new();
    for row in raw.split("\n") {
        let row = row.trim();
        let mut chars = row.chars();
        if !row.is_empty() {
            let start = chars.next().unwrap();
            if start == '|' {
                let command: String = chars.collect();
                let mut parts = command.split(" ");
                let command = parts.next().unwrap();
                let args: Vec<_> = parts.collect();
                run_command(&mut result, &mut plugins, command, args);
            } else {
                result.push_str(row);
                result.push('\n');
            }
        }
    }
    (result, plugins)
}
