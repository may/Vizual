use std::{
    collections::{HashMap, HashSet},
    fs,
};

use crate::{
    lexer::{self, Token, TokenType},
    parser::{self, Expression, Statement},
};

const HEADER: &str = "
#![allow(warnings, unused)]

use std::collections::HashMap;
use std::io::{self, Write};

use libviz::*;
use runtime::*;

struct Character {
    name: Str,
    sprites: Sprites
}
struct Character_Args {
    name: Option<Str>,
    sprites: Option<Sprites>
}

impl CanSpeaker for Character {
    fn speaker(&self) -> String {
        self.name.speaker()
    }
}
impl Character {
    fn new(args: Character_Args) -> Character {
        let name = args.name.unwrap();
        let sprites = args.sprites.unwrap_or_else(|| {
            Sprites::new(HashMap::new())
        });
        Character {
            name, sprites
        }
    }
}

struct Sprites {
    data: HashMap<String, Str>
}
impl Sprites {
    fn new(args: HashMap<String, Str>) -> Sprites {
        Sprites {
            data: args
        }
    }
}

fn input(channel: &Backend, prompt: Str) -> Str {
    channel.send(OutputEvent::Input { prompt: prompt.speaker() }).unwrap();
    let InputEvent::InputResponse { text } = channel.recv().unwrap() else {
        panic!(\"Wrong event\")
    };
    Str { data: text }
}

fn main() {
    create_game(\"Vizual\", worker);
}

";

const MAIN_START: &str = "

fn worker(channel: Backend) {

";

const MAIN_END: &str = "

channel.send(OutputEvent::Exit).unwrap();

}

";

enum Part {
    String(String),
    Value(String),
}

fn parse_chunks(text: &str) -> Vec<Part> {
    let mut result = vec![];
    let mut inside = String::new();
    let mut outside = String::new();
    let mut level = 0;

    for chr in text.chars() {
        if chr == '{' {
            level += 1;
            if level != 1 {
                inside.push(chr);
            } else if level == 1 {
                result.push(Part::String(outside.clone()));
                outside.clear();
            }
        } else if chr == '}' {
            level -= 1;
            if level == 0 {
                result.push(Part::Value(inside.clone()));
                inside.clear();
            } else {
                inside.push(chr);
            }
        } else if level == 0 {
            outside.push(chr);
        } else {
            inside.push(chr);
        }
    }

    result.push(Part::String(outside));

    result
}

fn recompile(raw: String, ctx: &mut Context) -> String {
    let mut tokens = lexer::lex(&raw);
    tokens.push(Token {
        tt: TokenType::Semicolon,
        row: 0,
        col: 0,
    });
    let mut p = parser::Parser::new(tokens);
    let res = p.parse_expression();
    render_expression(res, ctx)
}

fn render_expression(expr: Expression, ctx: &mut Context) -> String {
    match expr {
        Expression::String { value } => {
            let template_string = parse_chunks(&value);
            let formatter_template = template_string
                .iter()
                .map(|_| "{}")
                .collect::<Vec<_>>()
                .join("");
            let formatter_args = template_string
                .iter()
                .map(|x| match x {
                    Part::String(value) => format!("\"{}\"", value.escape_default()),
                    Part::Value(value) => format!("{}.speech()", recompile(value.clone(), ctx)),
                })
                .collect::<Vec<_>>()
                .join(",");
            format!(
                "Str {{ data: format!(\"{}\", {}) }}",
                formatter_template, formatter_args
            )
        }
        Expression::BinaryOp { lhs, operator, rhs } => {
            let lhs = render_expression(*lhs, ctx);
            let rhs = render_expression(*rhs, ctx);
            format!("{}{}{}", lhs, operator.repr(), rhs)
        }
        Expression::Integer { value } => {
            format!("Int {{ data: {} }}", value)
        }
        Expression::Defer { inner } => {
            ctx.is_deferred = true;
            let result = render(inner, ctx);
            ctx.is_deferred = false;
            format!(
                "channel.send( OutputEvent::DeferredExecute{{ func: Box::new( || {{  {}  }}  )}})",
                result
            )
        }
        Expression::TypeCall { item, arguments } => {
            let pattern = render_expression(*item, ctx);
            let definition = ctx.structs.get(&pattern).unwrap();

            match definition {
                NewStruct::Args(arg_list) => {
                    let mut args = String::new();
                    let mut remaining: HashSet<String> =
                        HashSet::from_iter(arg_list.iter().cloned());

                    for (k, v) in arguments {
                        if !remaining.contains(&k) {
                            panic!("{} does not accept argument {}", pattern, k)
                        }
                        remaining.remove(&k);
                        args.push_str(&format!("{}: Some({}),", k, render_expression(v, ctx)));
                    }

                    for k in remaining {
                        args.push_str(&format!("{}: None,", k));
                    }

                    let constructor = format!("{}_Args {{ {} }}", pattern, args);
                    format!("{}::new({})", pattern, constructor)
                }
                NewStruct::Variadic => {
                    let mut args = String::new();
                    for (k, v) in arguments {
                        args.push_str(&format!(
                            "(\"{}\".to_string(), {}), ",
                            k,
                            render_expression(v, ctx)
                        ));
                    }
                    format!("{}::new(HashMap::from([{}]))", pattern, args)
                }
            }
        }
        Expression::For {
            binding,
            iterator,
            inner,
        } => {
            let bind = render_expression(*binding, ctx);
            let iter = render_expression(*iterator, ctx);
            let result = render(inner, ctx);
            format!("for ({}) in ({}) {{  {}  }}", bind, iter, result)
        }
        Expression::If { condition, inner } => {
            let cond = render_expression(*condition, ctx);
            let result = render(inner, ctx);
            format!("if ({}) {{  {}  }}", cond, result)
        }
        Expression::Menu { options } => {
            let mut fin = String::new();
            let mut results = vec![];
            fin.push_str("channel.send(OutputEvent::Menu { options: vec![");
            for (key, result) in options.iter() {
                let key = render_expression(key.clone(), ctx);
                let result = match result.clone() {
                    Statement::Expression { expr } => match *expr {
                        Expression::FunctionDef { inner } => {
                            let inner_code = render(inner, ctx);
                            format!("{}()", inner_code)
                        }
                        _ => render(vec![result.clone()], ctx),
                    },
                    _ => render(vec![result.clone()], ctx),
                };
                results.push(result);
                fin.push_str(&format!("{}.speech(),", key));
            }
            fin.push_str("]}).unwrap();");
            fin.push_str(
                "
let inp = {
    let InputEvent::MenuChoice { index } = channel.recv().unwrap() else {
        panic!(\"Wrong response type\")
    };
    index
};
",
            );
            fin.push_str("match inp {");
            for (i, result) in results.iter().enumerate() {
                fin.push_str(&format!("{} => {{ {} }}", i, result));
            }
            fin.push_str("_ => panic!() }");
            fin
        }
        Expression::FunctionCall { item, arguments } => {
            let args = arguments
                .iter()
                .map(|x| render_expression(x.clone(), ctx))
                .collect::<Vec<_>>()
                .join(",");
            let pattern = render_expression(*item, ctx);
            if ctx.is_deferred {
                format!("{}({})", pattern, args)
            } else {
                format!("{}(&channel, {})", pattern, args)
            }
        }
        Expression::FunctionDef { inner } => {
            let inner_code = render(inner, ctx);
            format!("|channel: Backend| {{  {}  }}", inner_code)
        }
        Expression::Identifier { value } => {
            if value.as_str() == "std" {
                "_std".to_string()
            } else if value.as_str() == "all" {
                "*".to_string()
            } else {
                value
            }
        }
        Expression::Use { target } => {
            format!("use {}\n", render_expression(*target, ctx))
        }
        _ => todo!(),
    }
}

fn render_node(node: Statement, ctx: &mut Context) -> String {
    match node {
        Statement::Speech { item, text } => {
            let speaker = format!("{}.speaker()", render_expression(*item, ctx));
            let speech = format!("{}.speech()", render_expression(*text, ctx));
            format!(
                "channel.send(OutputEvent::Speech {{  speaker: {}, text: {}  }}).unwrap(); \n channel.recv();",
                speaker, speech
            )
        }
        Statement::Assign { item, value } => {
            if let Expression::Identifier { value: ident } = *item {
                if ctx.declared_variables.contains(&ident) {
                    let value = render_expression(*value, ctx);
                    format!("{} = {};\n", ident, value)
                } else {
                    let value = render_expression(*value, ctx);
                    format!("let mut {} = {};\n", ident, value)
                }
            } else {
                let pattern = render_expression(*item, ctx);
                let value = render_expression(*value, ctx);
                format!("{} = {};\n", pattern, value)
            }
        }
        Statement::Expression { expr } => format!("{};", render_expression(*expr, ctx)),
    }
}

enum NewStruct {
    Args(Vec<String>),
    Variadic,
}

struct Context {
    declared_variables: HashSet<String>,
    structs: HashMap<String, NewStruct>,
    is_deferred: bool,
}

fn render(nodes: Vec<Statement>, ctx: &mut Context) -> String {
    let mut main = String::new();

    for node in nodes {
        let text = render_node(node, ctx);
        main.push_str(&text);
    }

    main
}

pub fn compile(nodes: Vec<Statement>, dest: &str, plugins: Vec<String>) {
    let mut result = String::new();
    result.push_str(HEADER);
    for plugin in plugins {
        if plugin.as_str() == "std" {
            result.push_str(&format!("use plugin_{}::plugin as _{};\n", plugin, plugin));
        } else {
            result.push_str(&format!("use plugin_{}::plugin as {};\n", plugin, plugin));
        }
    }
    result.push_str(MAIN_START);
    let mut ctx = Context {
        declared_variables: HashSet::new(),
        structs: HashMap::from([
            (
                "Character".to_string(),
                NewStruct::Args(vec!["name".to_string(), "sprites".to_string()]),
            ),
            ("Sprites".to_string(), NewStruct::Variadic),
        ]),
        is_deferred: false,
    };
    let main = render(nodes, &mut ctx);
    result.push_str(&main);
    result.push_str(MAIN_END);
    fs::write(dest, &result).unwrap();
}
