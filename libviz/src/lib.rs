pub use macros::plug;

use std::ops;

pub trait CanSpeaker {
    fn speaker(&self) -> String;
}

pub trait CanSpeech {
    fn speech(&self) -> String;
}

#[derive(Debug, Clone)]
pub struct Str {
    pub data: String,
}
impl CanSpeaker for Str {
    fn speaker(&self) -> String {
        self.data.clone()
    }
}
impl CanSpeech for Str {
    fn speech(&self) -> String {
        self.data.clone()
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Int {
    pub data: i64,
}
impl CanSpeaker for Int {
    fn speaker(&self) -> String {
        self.data.to_string()
    }
}
impl CanSpeech for Int {
    fn speech(&self) -> String {
        self.data.to_string()
    }
}

#[derive(Debug)]
pub struct List<T> {
    pub data: Vec<T>,
}
impl<T: CanSpeech> CanSpeech for List<T> {
    fn speech(&self) -> String {
        format!(
            "[{}]",
            self.data
                .iter()
                .map(|x| x.speech())
                .collect::<Vec<_>>()
                .join(", ")
        )
    }
}

macro_rules! impl_op {
    ($op:ident, $op2:ident, $actor:tt) => {
        impl ops::$op for Int {
            type Output = Int;
            fn $op2(self, rhs: Self) -> Self::Output {
                Int {
                    data: self.data $actor rhs.data,
                }
            }
        }
    };
}

macro_rules! impl_op_assign {
    ($op:ident, $op2:ident, $actor:tt) => {
        impl ops::$op for Int {
            fn $op2(&mut self, rhs: Self) {
                *self = Self {
                    data: self.data $actor rhs.data,
                }
            }
        }
    };
}

impl std::cmp::PartialEq for Int {
    fn eq(&self, other: &Self) -> bool {
        self.data == other.data
    }
}
impl std::cmp::Eq for Int {}

impl std::cmp::PartialOrd for Int {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.data.partial_cmp(&other.data)
    }
}
impl std::cmp::Ord for Int {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.data.cmp(&other.data)
    }
}

impl_op!(Add, add, +);
impl_op!(Sub, sub, -);
impl_op!(Mul, mul, *);
impl_op!(Div, div, /);

impl_op_assign!(AddAssign, add_assign, +);
impl_op_assign!(SubAssign, sub_assign, -);
impl_op_assign!(MulAssign, mul_assign, *);
impl_op_assign!(DivAssign, div_assign, /);

impl<T> IntoIterator for List<T> {
    type Item = T;
    type IntoIter = std::vec::IntoIter<T>;
    fn into_iter(self) -> Self::IntoIter {
        self.data.into_iter()
    }
}
