use proc_macro::TokenStream;
use proc_macro2::{Ident, Span};
use quote::quote;
use syn::{
    self,
    punctuated::Punctuated,
    token::{And, Colon},
    FnArg, ImplItem, ImplItemMethod, Item, ItemFn, ItemImpl, ItemMod, Pat, PatPath, PatType, Path,
    PathArguments, PathSegment, Type, TypePath, TypeReference, Visibility,
};

fn modify2(mut func: ImplItemMethod) -> ImplItemMethod {
    let item = func.clone();
    let mut params = item.sig.inputs.iter();
    let mut index = 0;

    if let Some(FnArg::Receiver(_)) = params.next() {
        index = 1
    }

    let mut pat = Punctuated::new();
    pat.push(PathSegment {
        ident: Ident::new("runtime", Span::call_site()),
        arguments: PathArguments::None,
    });
    pat.push(PathSegment {
        ident: Ident::new("Backend", Span::call_site()),
        arguments: PathArguments::None,
    });
    let final_type = Type::Reference(TypeReference {
        and_token: And {
            spans: [Span::call_site()],
        },
        lifetime: None,
        mutability: None,
        elem: Box::new(Type::Path(TypePath {
            qself: None,
            path: Path {
                leading_colon: None,
                segments: pat,
            },
        })),
    });

    let mut arg_name = Punctuated::new();
    arg_name.push(PathSegment {
        ident: Ident::new("_", Span::call_site()),
        arguments: PathArguments::None,
    });

    func.sig.inputs.insert(
        index,
        FnArg::Typed(PatType {
            attrs: vec![],
            pat: Box::new(Pat::Path(PatPath {
                attrs: vec![],
                qself: None,
                path: Path {
                    leading_colon: None,
                    segments: arg_name,
                },
            })),
            colon_token: Colon {
                spans: [Span::call_site()],
            },
            ty: Box::new(final_type),
        }),
    );
    func
}

fn modify(mut func: ItemFn) -> ItemFn {
    let item = func.clone();
    let mut params = item.sig.inputs.iter();
    let mut index = 0;

    if let Some(FnArg::Receiver(_)) = params.next() {
        index = 1
    }

    let mut pat = Punctuated::new();
    pat.push(PathSegment {
        ident: Ident::new("runtime", Span::call_site()),
        arguments: PathArguments::None,
    });
    pat.push(PathSegment {
        ident: Ident::new("Backend", Span::call_site()),
        arguments: PathArguments::None,
    });
    let final_type = Type::Reference(TypeReference {
        and_token: And {
            spans: [Span::call_site()],
        },
        lifetime: None,
        mutability: None,
        elem: Box::new(Type::Path(TypePath {
            qself: None,
            path: Path {
                leading_colon: None,
                segments: pat,
            },
        })),
    });

    let mut arg_name = Punctuated::new();
    arg_name.push(PathSegment {
        ident: Ident::new("_", Span::call_site()),
        arguments: PathArguments::None,
    });

    func.sig.inputs.insert(
        index,
        FnArg::Typed(PatType {
            attrs: vec![],
            pat: Box::new(Pat::Path(PatPath {
                attrs: vec![],
                qself: None,
                path: Path {
                    leading_colon: None,
                    segments: arg_name,
                },
            })),
            colon_token: Colon {
                spans: [Span::call_site()],
            },
            ty: Box::new(final_type),
        }),
    );
    func
}

fn modify_fn(func: ItemFn) -> Item {
    let item = func.clone();
    let mut params = item.sig.inputs.iter();
    let mut first_arg = params.next();

    if let Visibility::Public(_) = item.vis {
        if let Some(FnArg::Receiver(_)) = first_arg {
            first_arg = params.next();
        }

        if let Some(FnArg::Typed(pat_type)) = first_arg {
            let ty = *pat_type.ty.clone();
            if let Type::Reference(inner_type) = ty {
                if let Type::Path(inner) = *inner_type.elem {
                    let segments = inner.path.segments.iter().collect::<Vec<_>>();
                    let final_type = segments.last().unwrap();
                    if final_type.ident.to_string().as_str() == "Backend" {
                        Item::Fn(func)
                    } else {
                        Item::Fn(modify(func))
                    }
                } else {
                    Item::Fn(modify(func))
                }
            } else {
                Item::Fn(modify(func))
            }
        } else {
            Item::Fn(modify(func))
        }
    } else {
        Item::Fn(func)
    }
}

fn modify_method(func: ImplItemMethod) -> ImplItem {
    let item = func.clone();
    let mut params = item.sig.inputs.iter();
    let mut first_arg = params.next();

    if let Visibility::Public(_) = item.vis {
        if let Some(FnArg::Receiver(_)) = first_arg {
            first_arg = params.next();
        }

        if let Some(FnArg::Typed(pat_type)) = first_arg {
            let ty = *pat_type.ty.clone();
            if let Type::Reference(inner_type) = ty {
                if let Type::Path(inner) = *inner_type.elem {
                    let segments = inner.path.segments.iter().collect::<Vec<_>>();
                    let final_type = segments.last().unwrap();
                    if final_type.ident.to_string().as_str() == "Backend" {
                        ImplItem::Method(func)
                    } else {
                        ImplItem::Method(modify2(func))
                    }
                } else {
                    ImplItem::Method(modify2(func))
                }
            } else {
                ImplItem::Method(modify2(func))
            }
        } else {
            ImplItem::Method(modify2(func))
        }
    } else {
        ImplItem::Method(func)
    }
}

fn modify_item(item: ImplItem) -> ImplItem {
    match item {
        ImplItem::Method(method) => modify_method(method),
        _ => item,
    }
}

fn modify_impl(mut item: ItemImpl) -> Item {
    item.items = item
        .items
        .iter()
        .cloned()
        .map(|x| modify_item(x))
        .collect::<Vec<_>>();
    Item::Impl(item)
}

#[proc_macro_attribute]
pub fn plug(_: TokenStream, input: TokenStream) -> TokenStream {
    let ast: ItemMod = syn::parse(input).unwrap();
    let (_, items) = ast.content.unwrap();

    let mut result_items = vec![];

    for item in items {
        match item.clone() {
            Item::Fn(func) => result_items.push(modify_fn(func)),
            Item::Impl(block) => match block.trait_ {
                Some(_) => result_items.push(item),
                None => result_items.push(modify_impl(block)),
            },
            x @ _ => result_items.push(x),
        }
    }

    quote! {
        pub mod plugin {
            #(
                #result_items
            )*
        }
    }
    .into()
}
