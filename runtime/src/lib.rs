use std::{
    sync::mpsc::{channel, Receiver, Sender},
    thread,
};

use macroquad::prelude::*;

#[derive(Debug)]
pub enum InputEvent {
    MenuChoice { index: usize },
    InputResponse { text: String },
    Continue,
}

pub enum OutputEvent {
    Speech { speaker: String, text: String },
    Menu { options: Vec<String> },
    Input { prompt: String },
    DeferredExecute { func: Box<dyn Fn() + Send> },
    Exit,
}

pub struct Frontend {
    input_sender: Sender<InputEvent>,
    output_receiver: Receiver<OutputEvent>,
}

pub struct Backend {
    output_sender: Sender<OutputEvent>,
    input_receiver: Receiver<InputEvent>,
}

impl Frontend {
    pub fn send(&self, event: InputEvent) -> Result<(), std::sync::mpsc::SendError<InputEvent>> {
        self.input_sender.send(event)
    }
    pub fn recv(&self) -> Result<OutputEvent, std::sync::mpsc::RecvError> {
        self.output_receiver.recv()
    }
}

impl Backend {
    pub fn send(&self, event: OutputEvent) -> Result<(), std::sync::mpsc::SendError<OutputEvent>> {
        self.output_sender.send(event)
    }
    pub fn recv(&self) -> Result<InputEvent, std::sync::mpsc::RecvError> {
        self.input_receiver.recv()
    }
}

pub fn create_game(name: &str, worker: fn(Backend)) {
    macroquad::Window::new(name, start(worker));
}

async fn start(worker: fn(Backend)) {
    let (input_sender, input_receiver) = channel();
    let (output_sender, output_receiver) = channel();

    let frontend = Frontend {
        input_sender,
        output_receiver,
    };
    let backend = Backend {
        output_sender,
        input_receiver,
    };

    let handle = thread::spawn(move || {
        worker(backend);
    });

    game_loop(frontend).await;
    handle.join().unwrap();
}

async fn game_loop(channel: Frontend) {
    let mut last_event = channel.recv().unwrap();
    let mut menu_choice = 0;
    let mut input_text = String::new();
    loop {
        // Render step
        clear_background(BLACK);
        match &last_event {
            OutputEvent::Speech { speaker, text } => {
                let y_offset = screen_height() - 210.0;
                draw_rectangle(10.0, y_offset, screen_width() - 20.0, 200.0, WHITE);
                draw_text(speaker, 10.0, 23.0 + y_offset, 30.0, BLACK);
                draw_text(text, 10.0, 45.0 + y_offset, 25.0, BLACK);
            }
            OutputEvent::Menu { options } => {
                let y_offset = screen_height() - 210.0;
                draw_rectangle(10.0, y_offset, screen_width() - 20.0, 200.0, WHITE);
                for (i, option) in options.iter().enumerate() {
                    draw_text(option, 25.0, 30.0 + y_offset + i as f32 * 35.0, 30.0, BLACK);
                    if i == menu_choice {
                        draw_poly(10.0, 25.0 + y_offset + i as f32 * 35.0, 3, 10.0, 0.0, BLACK);
                    }
                }
            }
            OutputEvent::Input { prompt } => {
                let y_offset = screen_height() - 210.0;
                draw_rectangle(10.0, y_offset, screen_width() - 20.0, 200.0, WHITE);
                draw_text(prompt, 10.0, 23.0 + y_offset, 30.0, BLACK);
                draw_rectangle(14.0, y_offset + 30.0, screen_width() - 28.0, 60.0, BLACK);
                draw_rectangle(16.0, y_offset + 32.0, screen_width() - 32.0, 54.0, WHITE);
                draw_text(&input_text, 18.0, 62.0 + y_offset, 25.0, BLACK);
            }
            OutputEvent::Exit => {
                let y_offset = screen_height() - 210.0;
                draw_rectangle(10.0, y_offset, screen_width() - 20.0, 200.0, WHITE);
                draw_text("GAME COMPLETE", 10.0, 23.0 + y_offset, 30.0, BLACK);
            }
            OutputEvent::DeferredExecute { func } => {
                func();
                last_event = channel.recv().unwrap();
            }
        }

        // User input step
        if is_key_pressed(KeyCode::Space) {
            if let OutputEvent::Speech { .. } = last_event {
                channel.send(InputEvent::Continue).unwrap();
                last_event = channel.recv().unwrap();
            }
        } else if is_key_pressed(KeyCode::Up) {
            if let OutputEvent::Menu { .. } = last_event {
                menu_choice -= 1;
            }
        } else if is_key_pressed(KeyCode::Down) {
            if let OutputEvent::Menu { options } = &last_event {
                menu_choice += 1;
                menu_choice %= options.len();
            }
        } else if is_key_pressed(KeyCode::Enter) {
            if let OutputEvent::Menu { .. } = last_event {
                channel
                    .send(InputEvent::MenuChoice { index: menu_choice })
                    .unwrap();
                last_event = channel.recv().unwrap();
            }
        }

        if let OutputEvent::Input { .. } = last_event {
            let shift = is_key_down(KeyCode::LeftShift) || is_key_down(KeyCode::RightShift);
            if is_key_pressed(KeyCode::Enter) {
                channel
                    .send(InputEvent::InputResponse {
                        text: input_text.clone(),
                    })
                    .unwrap();
                last_event = channel.recv().unwrap();
            } else if is_key_pressed(KeyCode::Backspace) {
                input_text = input_text[0..input_text.len() - 1].to_string();
            } else if is_key_pressed(KeyCode::Space) {
                input_text.push(' ');
            } else if is_key_pressed(KeyCode::A) {
                char_pressed(&mut input_text, shift, 'a');
            } else if is_key_pressed(KeyCode::B) {
                char_pressed(&mut input_text, shift, 'b');
            } else if is_key_pressed(KeyCode::C) {
                char_pressed(&mut input_text, shift, 'c');
            } else if is_key_pressed(KeyCode::D) {
                char_pressed(&mut input_text, shift, 'd');
            } else if is_key_pressed(KeyCode::E) {
                char_pressed(&mut input_text, shift, 'e');
            } else if is_key_pressed(KeyCode::F) {
                char_pressed(&mut input_text, shift, 'f');
            } else if is_key_pressed(KeyCode::G) {
                char_pressed(&mut input_text, shift, 'g');
            } else if is_key_pressed(KeyCode::H) {
                char_pressed(&mut input_text, shift, 'h');
            } else if is_key_pressed(KeyCode::I) {
                char_pressed(&mut input_text, shift, 'i');
            } else if is_key_pressed(KeyCode::J) {
                char_pressed(&mut input_text, shift, 'j');
            } else if is_key_pressed(KeyCode::K) {
                char_pressed(&mut input_text, shift, 'k');
            } else if is_key_pressed(KeyCode::L) {
                char_pressed(&mut input_text, shift, 'l');
            } else if is_key_pressed(KeyCode::M) {
                char_pressed(&mut input_text, shift, 'm');
            } else if is_key_pressed(KeyCode::N) {
                char_pressed(&mut input_text, shift, 'n');
            } else if is_key_pressed(KeyCode::O) {
                char_pressed(&mut input_text, shift, 'o');
            } else if is_key_pressed(KeyCode::P) {
                char_pressed(&mut input_text, shift, 'p');
            } else if is_key_pressed(KeyCode::Q) {
                char_pressed(&mut input_text, shift, 'q');
            } else if is_key_pressed(KeyCode::R) {
                char_pressed(&mut input_text, shift, 'r');
            } else if is_key_pressed(KeyCode::S) {
                char_pressed(&mut input_text, shift, 's');
            } else if is_key_pressed(KeyCode::T) {
                char_pressed(&mut input_text, shift, 't');
            } else if is_key_pressed(KeyCode::U) {
                char_pressed(&mut input_text, shift, 'u');
            } else if is_key_pressed(KeyCode::V) {
                char_pressed(&mut input_text, shift, 'v');
            } else if is_key_pressed(KeyCode::W) {
                char_pressed(&mut input_text, shift, 'w');
            } else if is_key_pressed(KeyCode::X) {
                char_pressed(&mut input_text, shift, 'x');
            } else if is_key_pressed(KeyCode::Y) {
                char_pressed(&mut input_text, shift, 'y');
            } else if is_key_pressed(KeyCode::Z) {
                char_pressed(&mut input_text, shift, 'z');
            }
        }

        next_frame().await
    }
}

fn char_pressed(buf: &mut String, shift: bool, chr: char) {
    if shift {
        buf.push((chr as u8 - 32) as char);
    } else {
        buf.push(chr);
    }
}
