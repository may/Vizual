use runtime::{Backend, InputEvent, OutputEvent};

fn worker_thread(channel: Backend) {
    channel
        .send(OutputEvent::Speech {
            speaker: "Engine".to_string(),
            text: "Engine started".to_string(),
        })
        .unwrap();
    channel.recv().unwrap();
    channel
        .send(OutputEvent::Menu {
            options: vec![
                "Option 1".to_string(),
                "option 2".to_string(),
                "third option".to_string(),
            ],
        })
        .unwrap();
    let resp = channel.recv().unwrap();
    if let InputEvent::MenuChoice { index } = resp {
        channel
            .send(OutputEvent::Speech {
                speaker: "Engine".to_string(),
                text: format!("You chose option {}", index),
            })
            .unwrap();
        channel.recv().unwrap();
        channel
            .send(OutputEvent::Input {
                prompt: "question?".to_string(),
            })
            .unwrap();
        let resp = channel.recv().unwrap();
        if let InputEvent::InputResponse { text: sp } = resp {
            channel
                .send(OutputEvent::Speech {
                    speaker: "Engine".to_string(),
                    text: format!("You said {}", sp),
                })
                .unwrap();
            channel.recv().unwrap();
        }
    }
    loop {}
}

fn main() {
    runtime::create_game("Vizual", worker_thread);
}
